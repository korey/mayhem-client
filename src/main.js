// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import vueResource from 'vue-resource'
Vue.use(vueResource)


import Toasted from 'vue-toasted'; //https://www.npmjs.com/package/vue-toasted
Vue.use(Toasted) 

import ToggleButton from 'vue-js-toggle-button' //https://github.com/euvl/vue-js-toggle-button
Vue.use(ToggleButton)


import VueSweetAlert from 'vue-sweetalert'
Vue.use(VueSweetAlert)

import VueStash from 'vue-stash'
Vue.use(VueStash)

import Auth from './plugins/auth.js'
Vue.use(Auth)


Vue.http.interceptors.push(function(req, next) {
  if(req.url[0] === '/') {
    //Env varibles are defined in /config/dev.env.js or prod.env.js
    req.url = process.env.API + req.url

    let token = Vue.prototype.$getToken()

    if(token) {
      req.headers.set('x-access-token', token)
    }
  }

  next(function(res) {

  })
})

//define Router Guards

router.beforeEach(function (to ,from , next){
  //prevent access to routes that require 'guest'
  if (to.matched.some(function(record) { return record.meta.requiresGuest}) && Vue.prototype.$loggedIn()) {
      next({ path: '/newsfeed' })
  } else if (to.matched.some(function(record) { return record.meta.requireAuth}) && !Vue.prototype.$loggedIn()) {
      next({
        path: `/login?redirect=${to.fullPath}`
      })
  } else {
    next()
  }
})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  data: {
    store: {
      departments : [],
      user: {},
      showAlert: false,
      showAlertMsg : "",
      showAlertClass: "success",
      from: {},
      loading: false
    }
  },
  router,
  template: '<App/>',
  components: { App },
  created () {

  },
  methods: {

  }
})
