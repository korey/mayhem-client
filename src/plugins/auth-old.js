var AuthPlugin = {
  setToken: function (token, exp) {
    localStorage.setItem('authToken', token)
    localStorage.setItem('authTokenExp', exp)
  },
  destroyToken: function () {
    localStorage.removeItem('authToken')
    localStorage.removeItem('authTokenExp')
  },
  getToken: function () {
    let token = localStorage.getItem('authToken')
    let exp = localStorage.getItem('authTokenExp')

    if (!token || !exp) {
      return null
    }

    if (Date.now() > parseInt(exp)) {
      this.destroyToken()
      return null
    } else {
      return token
    }
  },
  loggedIn: function () {
    if (this.getToken()) {
      return true
    } else {
      return false
    }
  }
}

export default function (Vue) {
  Vue.auth = AuthPlugin

  Object.defineProperties(Vue.prototype, {
    $auth: function () {
      return Vue.auth
    }
  })
}
