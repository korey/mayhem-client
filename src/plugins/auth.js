const AuthPlugin = {
  install(Vue, options) {

    Vue.prototype.$setToken = function (token, exp) {
      localStorage.setItem('authToken', token)
      localStorage.setItem('authTokenExp', exp)
    }

    Vue.prototype.$destroyToken = function () {
      localStorage.removeItem('authToken')
      localStorage.removeItem('authTokenExp')
    }

    Vue.prototype.$getToken = function () {
      let token = localStorage.getItem('authToken')
      let exp = localStorage.getItem('authTokenExp')

      if (!token || !exp) {
        return null
      }

      if (Date.now() > parseInt(exp)) {
        this.$destroyToken()
        return null
      } else {
        return token
      }
    }

    Vue.prototype.$loggedIn = function () {
      if (this.$getToken()) {
        return true
      } else {
        return false
      }
    }
  }
}

export default AuthPlugin;
