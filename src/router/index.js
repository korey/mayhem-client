import Vue from 'vue'
import Router from 'vue-router'
import Dash from 'components/dashboard'
import Departments from 'components/departments'
import Department from 'components/department'
import Login from 'components/auth/login'
import Newsfeed from 'components/newsfeed'
import Venders from 'components/venders'
import Vender from 'components/vender'
import VenderAdd from 'components/venderAdd'
import Resources from 'components/resources'
import Resource from 'components/resource'
import ResourceAdd from 'components/resourceAdd'
import businessProcesses from 'components/business-processes'
import businessProcess from 'components/business-process'
import businessProcessAdd from 'components/business-processAdd'
import bugAdd from 'components/bugAdd'
import Personel from 'components/personel'
import Person from 'components/person'
import PersonAdd from 'components/personAdd'
import Sites from 'components/sites'
import Site from 'components/site'
import SiteAdd from 'components/siteAdd'
import deptPersonel from 'components/deptPersonel'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Dash,
      redirect: '/newsfeed',
      children: [
        {
          path: 'newsfeed',
          component: Newsfeed,
          meta: {requireAuth: true}
        },
        {
          path: 'departments',
          component: Departments,
          meta: {requireAuth: true}
        },
        {
          path: 'department/:deptId',
          name: 'department',
          component: Department,
          meta: {requireAuth: true},
        },
        {
          path: 'deptPersonel/:deptId',
          name: 'deptPersonel',
          component: deptPersonel,
          meta: {requireAuth: true}
        },
        {
          path: 'vendors',
          name: 'vendors',
          component: Venders,
          meta: {requireAuth: true}
        },
        {
          path: 'vendor/:venderId',
          name: 'vendor',
          component: Vender,
          meta: {requireAuth: true}
        },
        {
          path: 'venderAdd',
          name: 'venderAdd',
          component: VenderAdd,
          meta: {requireAuth: true}
        },
        {
          path: 'resources',
          name: 'resources',
          component: Resources,
          meta: {requireAuth: true}
        },
        {
          path: 'resource/:resourceId',
          name: 'resource',
          component: Resource,
          meta: {requireAuth: true}
        },
        {
          path: 'resourceAdd',
          name: 'resourceAdd',
          component: ResourceAdd,
          meta: {requireAuth: true}
        },
        {
          path: 'personel',
          name: 'personel',
          component: Personel,
          meta: {requireAuth: true}
        },
        {
          path: 'person/:personId',
          name: 'person',
          component: Person,
          meta: {requireAuth: true}
        },
        {
          path: 'personAdd',
          name: 'personAdd',
          component: PersonAdd,
          meta: {requireAuth: true}
        },
        {
          path: 'sites',
          name: 'sites',
          component: Sites,
          meta: {requireAuth: true}
        },
        {
          path: 'site/:siteId',
          name: 'site',
          component: Site,
          meta: {requireAuth: true}
        },
        {
          path: 'siteAdd',
          name: 'siteAdd',
          component: SiteAdd,
          meta: {requireAuth: true}
        },
        {
          path: 'business-processes',
          name: 'business-processes',
          component: businessProcesses,
          meta: {requireAuth: true}
        },
        {
          path: 'business-process/:bpid',
          name: 'business-process',
          component: businessProcess,
          meta: {requireAuth: true}
        },
        {
          path: 'business-processAdd',
          name: 'business-processAdd',
          component: businessProcessAdd,
          meta: {requireAuth: true}
        },{
          path: 'bugAdd',
          component: bugAdd,
          meta: {requireAuth: true}
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: { requiresGuest: true }
    }
  ]
})
